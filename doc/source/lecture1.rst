====================
  Tutorial 1 
====================


Basic pyuni10
=============
The goal of this part of the tutorial is to familarize the user with basic functionality of pyuni10.
The purpose is just to get you familiar with how things are done with pyuni10 in the code before moving to more complicated algorithms.

You can download the ipython notebook for this part of the tutorial: :download:`Tutorial1-1.ipynb<../../python/lecture1/Tutorial1-1.ipynb>`.



iTEBD
=====

We will implement the infinite time evolution bond decimation (iTEBD) method for the 1D transverse Ising model

.. math:: 
   
   H=\sum_{\langle ij\rangle}\sigma_i^z \sigma_j^z + h \sum_i \sigma^x_i

We will use the infinite Matrix Product State, represented in the conanical form as :

  .. image:: Figures/iMPS.jpg
     :width: 350pt


where :math:`\Gamma^{[A]_{S_A}}` and :math:`\Gamma^{[A]_{S_B}}` are :math:`\chi\times\chi` matrices on the A and B sublattices, 
and :math:`\lambda^{A}` and :math:`\lambda^{B}` are Schmidt values.


To update the matrices, we use the imaginary time evolution

.. math:: 
    
    |\Psi_\tau\rangle =
    \frac{\exp(-H\tau)|\Psi_0\rangle}{\|\exp(-H\tau)|\Psi_0\rangle\|}.


Using Suzuki-Trotter decomposition, the two-site imaginary time evolution 
operator is given by

.. math:: 
   
   U^{[r,r+1]} \equiv \exp(-ih^{[r,r+1]}\delta t),  \quad \delta t \ll 1,

To perform updates, we separate the Hamiltonian into :math:`h_{AB}` and 
:math:`h_{BA}` and apply :math:`U_{AB}` and :math:`U_{BA}` alternatively.

  .. image:: Figures/AB.jpg
     :width: 350pt

The update process is carried out by contracting :math:`U_{AB}`, and perform 
SVD of the resulting tensor :math:`\Theta` to obtain new :math:`\Gamma_A, 
\Gamma_B`, and :math:`\lambda_A` 

  .. image:: Figures/Theta.jpg
     :width: 350pt

and also for :math:`U_{BA}`


We repeat the process until the state is converged.



You can download the ipython notebook for this part of the tutorial: :download:`Tutorial1-2.ipynb<../../python/lecture1/Tutorial1-2.ipynb>`.


References
==========

Martix Procut States
--------------------

General references on matrix product states, tensor network states

  1. Ulrich Schollwoeck, The density-matrix renormalization group in the age of matrix product states, `Annals of Physics 326, 96 (2011) <http://www.sciencedirect.com/science/article/pii/S0003491610001752>`_ , http://arxiv.org/abs/1008.3477
  
  2. R. Orús, A Practical Introduction to Tensor Networks: Matrix Product States and Projected Entangled Pair States, `Annals of Physics 349 117 (2014) <http://www.sciencedirect.com/science/article/pii/S0003491614001596>`_, http://arxiv.org/abs/1306.2164

iTEBD
-----

  1. G. Vidal, Classical Simulation of Infinite-Size Quantum Lattice Systems in One Spatial Dimension, `Phys. Rev. Lett. 98, 070201 (2007)  <http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.98.070201>`_ , http://arxiv.org/abs/cond-mat/0605597

  2. R. Orús and G. Vidal, Infinite time-evolving block decimation algorithm beyond unitary evolution, `Phys. Rev. B 78, 155117 (2008) <http://journals.aps.org/prb/abstract/10.1103/PhysRevB.78.155117>`_ , http://arxiv.org/abs/0711.3960


.. X .. warning:: Under Construction!!
