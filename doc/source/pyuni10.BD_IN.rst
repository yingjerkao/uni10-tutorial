pyuni10.BD_IN
=============

.. py:data:: pyuni10.BD_IN = 1

   The value defines an **in-coming** bond
