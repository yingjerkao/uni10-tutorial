===================
Tutorial 3
===================

In this part of the tutorial, we will extend the ideas in one dimension to two dimensions. 



2D iTEBD
===================

We will construct a 2D version of imaginary-time iTEBD and apply it to obtain the ground state wave functions in the 2D transverse field Ising model (TFIM).

.. math:: 
   H=\sum_{\langle ij\rangle}\sigma_i^z \sigma_j^z + h \sum_i \sigma^x_i


We will extend the 1D iTEBD method to 2D.  The sites are updated in pairs and the bonds are updated in the sequence of the four bonds shown in red in the following figure. 

  .. image:: Figures/2DiTEBD.jpg
     :width: 450 pt

Example
-------

In the following, we give an example of the coordination number :math:`z=4` for the virtual bonds of each tensor.  We start with two tensors **GA** and **GB**, and diagonal matrices **Ls** on each bond:

  .. image:: Figures/3-1.jpg
     :width: 300 pt

We want to update first bond 0 with  **Ls[0]**. We first absorb **Ls** into the tensors (indicated as 
the  red bonds.)

  .. image:: Figures/3-2.jpg
     :width: 300 pt

We merge the bonds not being updated to a huge bond with bond dimensions :math:`\chi^3`

  .. image:: Figures/3-3.jpg
     :width: 300 pt

Relabeling the tensors so that pyuni10 knows how to contract them:

  .. image:: Figures/3-4.jpg
     :width: 300 pt

Finally, we obtain a :math:`\Theta` tensor of dimensions :math:`d \chi^3 \times d\chi^3`  by contracting
**GA**, **GB** and **U**

  .. image:: Figures/3-5.jpg
     :width: 300 pt

Performing SVD on  :math:`\Theta` and truncate the bond, we have three new tensors 

  * **U** ( :math:`d \chi^3 \times \chi` )
  * **Ls[0]** ( :math:`\chi \times \chi` ), and 
  * **V** :sup:`t` ( :math:`\chi \times d \chi^3` )

  .. image:: Figures/3-6.jpg
     :width: 250 pt

Thus we have new **GA** and **GB** merged with **Ls[1]**, **Ls[2]** and **Ls[3]**, and 
a new updated diagonal matrice **Ls[0]**.

  .. image:: Figures/3-7.jpg
     :width: 150 pt

To obtain **GA**, **GB**, we need to multiply the bonds with  **Ls[i]** :sup:`-1`

  .. image:: Figures/3-8.jpg
     :width: 300 pt

Repeat the same processes for the other bonds 1,2,and 3 to complete a single update. 

You can download the ipython notebook :download:`Tutorial3-1.ipynb  <../../python/lecture3//Tutorial3-1.ipynb>`.

Tensor Renormalization Group
======================================
We will implement the tensor renormalization group (TRG) algorithm to compute the energy and magnetization in TFIM. 

To compute the ground state expectation value for an operator :math:`\hat{O}`
 
  .. math::
  
     \langle \hat{O} \rangle =\frac{\langle \psi_0| \hat{O} |\psi_0 \rangle}{\langle \psi_0| \psi_0 \rangle }
       
 
To calculate the norm :math:`\langle \psi_0| \psi_0 \rangle` in the denominator,  
we contract first the physical indices of the bra and ket tensors to get a double tensor **T** :

  .. image:: Figures/Dtensor.jpg
     :width: 200pt 

For the numerator, we construct an *impurity* tensor **T'** :
 

  .. image:: Figures/Ditensor.jpg
     :width: 200pt

The numerator corresponds to a tensor network like this:

  .. image:: Figures/TRG_1.jpg
     :width: 150pt

To contract the tensor network using TRG, first decompose the A-site and B-site tensors 
into two rank-3 tensors using SVD, and perform truncation:

  .. image:: Figures/SVD_1.jpg
     :width: 200pt
  
  .. image:: Figures/SVD_2.jpg
     :width: 200pt
 
and we obtain,
 
  .. image:: Figures/TRG_2.jpg
     :width: 150pt

Contracting the tensors in the squares, we obtain a new tensor network with half of the size
   
  .. image:: Figures/TRG_3.jpg
     :width: 150pt

Repeat the process until only four tensors remain, and contract them to obtain 
:math:`\langle \psi_0| \hat{O} |\psi_0 \rangle`
  
  .. image:: Figures/TRG_4.jpg 
     :width: 120pt

Similarly, for  :math:`\langle \psi_0| \psi_0 \rangle`

  .. image:: Figures/TRG_5.jpg
     :width: 120pt


You can download the ipython notebook :download:`Tutorial3-2.ipynb  <../../python/lecture3//Tutorial3-2.ipynb>`.

You also need these network files: :download:`TRG.net <../../python/lecture3/TRG.net>` and
:download:`expectation.net <../../python/lecture3/expectation.net>`.

The labels for the tensors in the helper functions can be found :download:`here <../../python/lecture3/TRG.pdf>`.


The full python code for the 2D transverse Ising model can be found 
:download:`here <../../python/lecture3/ising_trg.py>`.

Refereces
=========

  1. J. Jordan, R. Orús, G. Vidal, F. Verstraete, and J. I. Cirac, 
     `Phys. Rev. Lett. 101, 250602 (2008) <http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.101.250602>`_, http://arxiv.org/abs/cond-mat/0703788

  2. H. C. Jiang, Z. Y. Weng, and T. Xiang, Accurate Determination of Tensor Network State of Quantum Lattice Models in Two Dimensions, 
     `Phys. Rev. Lett. 101, 090603 (2008) <http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.101.090603>`_, http://arxiv.org/abs/0806.3719

  3. H. H. Zhao, Z. Y. Xie, Q. N. Chen, Z. C. Wei, J. W. Cai, T. Xiang, Renormalization of Tensor-network States, 
     `Phys. Rev. B 81, 174411 (2010) <http://journals.aps.org/prb/abstract/10.1103/PhysRevB.81.174411>`_, http://arxiv.org/abs/1002.1405


.. X .. warning:: Under Construction!!
