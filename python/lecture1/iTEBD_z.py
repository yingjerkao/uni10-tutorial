import numpy as np
import math
import time


def gs(H,G,l,z,chi_bond,chi,delta_list,N):
    TEST = False
    
    # Generate the two-site time evolution operator   
    w,v = np.linalg.eig(H)   
    d = np.sqrt(H.shape[0])
    
    # Perform the imaginary time evolution alternating on A and B bonds
    for delta in delta_list:
        U = np.reshape(np.dot(np.dot(v,np.diag(np.exp(-delta*(w)))),np.linalg.inv(v)),(d,d,d,d))
        E_old = 1
        if(G.dtype!=complex):
            U=np.real(U)
            
        for step in range(0, N):
            E=0
            for bond in range(0,z):
                # Decorate Tensors with lambdas
                t0=time.time()
                
                G_A_l = G[0]
                for b in range(z-1,-1,-1):
                    G_A_l = np.tensordot(G_A_l,np.diag(l[np.mod(bond+b,z),:]),axes=(z,0))
                    G_A_l = np.transpose( G_A_l,np.hstack((0, np.mod(range(-1,z-1),z)+1)))

                G_B_l = G[1]
                for b in range(z-1,0,-1):
                    G_B_l = np.tensordot(G_B_l,np.diag(l[np.mod(bond+b,z),:]),axes=(z,0))
                    G_B_l = np.transpose( G_B_l,np.hstack((0, np.mod(range(-1,z-1),z)+1)))
                G_B_l = np.transpose( G_B_l,np.hstack((0, np.mod(range(-1,z-1),z)+1))) 

                if TEST==True:
                    print "DEC:",time.time()-t0,

                # Construct theta
                t0=time.time()
                
                theta = np.tensordot(G_A_l, G_B_l,axes=(1,1))
                theta = np.tensordot(U,theta,axes=([2,3],[0,z]))
                theta = np.reshape(np.transpose(theta,np.hstack((0, range(2,z+1), 1, range(z+1,2*z)))),(d*chi**(z-1),d*chi**(z-1)))

                if TEST==True:
                    print "THE:",time.time()-t0,
                # SVD
                t0=time.time()
                
                try:
                    X, Y, Z = np.linalg.svd(theta,full_matrices=0)
                except np.linalg.linalg.LinAlgError:
                    print 'SVD did not converge, diagonalizing A_dagger*A'
                    Y, Z = np.linalg.eigh(np.dot(theta.conj().T,theta))
                    piv = np.argsort(Y)[::-1]        
                    Y = np.sqrt(np.abs(Y[piv]))
                    Z = np.conj(Z[:,piv].T)
                    X = np.dot(np.dot(theta,np.conj(Z.T)),np.diag(Y**(-1)))
                Z = Z.T

                chi_new=np.min([chi,Y[Y>10**(-10)].shape[0]])
                chi_bond[bond] = chi_new

                if TEST==True:
                    print "SVD:",time.time()-t0,

                # Get the new Tensors
                t0=time.time()
                
                l[bond,0:chi_new]=Y[0:chi_new]/np.sqrt(sum(Y[0:chi_new]**2))

                G_new = np.reshape(X[0:d*chi**(z-1),0:chi], np.hstack((d, chi*np.ones(z) )))
                for b in range(1,z):
                    l_now = np.zeros(chi)
                    l_now[0:chi_bond[np.mod(bond+b,z)]] = l[np.mod(bond+b,z),0:chi_bond[np.mod(bond+b,z)]]**(-1)
                    G_new = np.tensordot(G_new,np.diag(l_now),axes=(1,1))
                G_new = np.transpose(G_new, np.hstack((0, np.mod(range(1,z+1),z)+1)))
                G_new = G_new[np.array([0,4,1,5,8,12,9,13,2,6,3,7,10,14,11,15])]
                G[0][:]=G_new

                    
                G_new = np.reshape(Z[0:d*chi**(z-1),0:chi], np.hstack((d, chi*np.ones(z) )))
                for b in range(1,z):
                    l_now = np.zeros(chi)
                    l_now[0:chi_bond[np.mod(bond+b,z)]] = l[np.mod(bond+b,z),0:chi_bond[np.mod(bond+b,z)]]**(-1)
                    G_new = np.tensordot(G_new,np.diag(l_now),axes=(1,1))
                G_new = np.transpose(G_new, np.hstack((0, np.mod(range(1,z+1),z)+1)))
                G_new = G_new[np.array([0,4,1,5,8,12,9,13,2,6,3,7,10,14,11,15])]
                G[1][:]=G_new

                if TEST==True:
                    print "NEW:",time.time()-t0

                E += -np.log(np.sum(theta*np.conj(theta)))/delta/4.0
            # Converged?
            if(np.mod(step,50)==0):
                Ebond, Nbond= bond_expectation_value(G,l,np.reshape(H,(d,d,d,d)))
                E = np.mean(Ebond)/np.mean(Nbond)*z/2.0/4.0
                if(np.abs(E_old-E)<10.0**(-12)):               
                    break
                E_old = E
        if step>=N-1:
            E = np.mean(Ebond)/np.mean(Nbond)*z/2.0/4.0
        print "  delta = %0.6f"%delta,"%6.0f"%step,"steps : E=%.10f"%np.real(E)
    return step


def site_expectation_value(G,l,z,O):
    # Calc <Sz>
    E=[]
    N=[]
    for site in range(0,2):
        G_l = G[site]
        for bond in range(z-1,-1,-1):
            G_l = np.tensordot(G_l,np.diag(l[bond,:]),axes=(z,0))
            G_l = np.transpose(G_l, np.hstack((0, np.mod(range(-1,z-1),z)+1)))

       
        E.append(np.sum(np.tensordot(O,G_l,(1,0))*np.conj(G_l)))
        N.append(np.sum(G_l*np.conj(G_l)))       
    return E,N


def bond_expectation_value(G,l,O):
    # Calc <Sz>
    E=[]
    N=[]
    z=l.shape[0]
    for bond in range(0,z):
        G_A_l = G[0]
        for b in range(z-1,-1,-1):
            G_A_l = np.tensordot(G_A_l,np.diag(l[np.mod(bond+b,z),:]),axes=(z,0))
            G_A_l = np.transpose( G_A_l,np.hstack((0, np.mod(range(-1,z-1),z)+1)))

        G_B_l = G[1]
        for b in range(z-1,0,-1):
            G_B_l = np.tensordot(G_B_l,np.diag(l[np.mod(bond+b,z),:]),axes=(z,0))
            G_B_l = np.transpose( G_B_l,np.hstack((0, np.mod(range(-1,z-1),z)+1)))
        G_B_l = np.transpose( G_B_l,np.hstack((0, np.mod(range(-1,z-1),z)+1)))

        # Construct theta
        theta = np.tensordot(G_A_l, G_B_l,axes=(1,1))
        theta = np.transpose(theta,np.hstack((0, z, range(1,z), range(z+1,2*z))))
        Otheta = np.tensordot(O,theta,axes=([0,1],[0,1]))
        N.append(np.sum(theta*np.conj(theta)))
        E.append(np.sum(Otheta*np.conj(theta)))
    return E,N
